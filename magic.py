#!/usr/bin/env python3

import click
import re
import hashlib
import yaml
import os
from debian.deb822 import Changes as DebChanges, Dsc as DebDsc

"""
Magic git tagger
"""


def get_inline_signature(filename: str) -> (str, [str]):
    """
    Given a signed file, extract the gpg signature and hash
    """
    signature = []
    hash = ""
    line_is_signature = False
    with open(filename, "r") as fp:
        for cnt, line in enumerate(fp):
            # Check that the first line has the GPG/PGP marker
            if cnt == 0:
                if "BEGIN PGP SIGNED MESSAGE" not in line:
                    raise Exception("file doesn't look like it is signed")
            if cnt == 1:
                hashmatch = re.match("^Hash: (.*)$", line)
                if hashmatch is None:
                    raise Exception("gnupg hash is not specified")
                hash = hashmatch[1]
            # The signature is everything between BEGIN and END of PGP SIGNATURE, included
            if "BEGIN PGP SIGNATURE" in line:
                line_is_signature = True
            if line_is_signature:
                signature.append(line.strip())
            if "END PGP SIGNATURE" in line:
                line_is_signature = False

    return (hash, signature)


def apply_inline_signature(
    filename: str, hash: str, signature: str, dgit_line: str = ""
) -> str:
    """
    Given an unsigned file, apply the gpg signature and hash
    """
    signed_str = "-----BEGIN PGP SIGNED MESSAGE-----\n"
    signed_str += "Hash: {}\n".format(hash)
    signed_str += "\n"
    with open(filename) as fp:
        for cnt, line in enumerate(fp):
            signed_str += line
    if dgit_line:
        signed_str += dgit_line + "\n"

    signed_str += "\n"
    signed_str += "\n".join(signature)

    return signed_str + "\n"


@click.group()
def cli():
    pass


@cli.command()
@click.argument("signed-changes-file", type=click.Path(exists=True))
@click.option("--message", '-m', type=str)
def sigs_from_changes(signed_changes_file, message):
    """
    Extract signature and hash from signed changes and dsc files
    to yaml
    """
    (hash, signature) = get_inline_signature(signed_changes_file)
    struct = {"DTag-changes-Hash": hash, "DTag-changes-Signature": signature}
    with open(signed_changes_file, "r") as fp:
        # parse .changes file with deb822.Changes
        rel = DebChanges(fp)
        dscfilename = next(
            r["name"] for r in rel["files"] if r["name"].endswith(".dsc")
        )
        signed_dsc_file = os.path.join(
            os.path.dirname(signed_changes_file), dscfilename
        )
        (hash, signature) = get_inline_signature(signed_dsc_file)
        struct.update({"DTag-dsc-Hash": hash, "DTag-dsc-Signature": signature})
    if message:
        print(message)
        print("---")
    print(yaml.dump(struct))


@click.option("--dgit-sha")
@click.argument("metadata", type=dict)
@click.argument("unsigned-dsc-file", type=click.Path(exists=True))
def sign_dsc_from_sigs(dgit_sha, metadata, unsigned_dsc_file):
    """
    Apply signature and hash from yaml to dsc
    """
    hash = metadata["DTag-dsc-Hash"]
    signature = metadata["DTag-dsc-Signature"]
    dgit_line = ""
    if dgit_sha:
        with open(unsigned_dsc_file, "r") as fp:
            # parse .changes file with deb822.Changes
            rel = DebDsc(fp)
            source = rel["Source"]
            version = rel["Version"]
        dgit_line = (
            "Dgit: {} debian archive/debian/{} https://git.dgit.debian.org/{}".format(
                dgit_sha, version, source
            )
        )
    signed_dsc_str = apply_inline_signature(
        unsigned_dsc_file, hash, signature, dgit_line
    )
    with open(unsigned_dsc_file, "w") as fp:
        fp.write(signed_dsc_str)


@click.argument("changes-file", type=click.Path(exists=True))
@click.argument("dsc-file", type=click.Path(exists=True))
def update_changes_with_dsc(changes_file, dsc_file):
    """
    Update changes file with the dsc file checksums
    """
    dsc_file_name = os.path.basename(dsc_file)
    dsc_file_size = os.path.getsize(dsc_file)
    # method to Changes general field
    hashes = {"md5": "Files", "sha1": "Checksums-Sha1", "sha256": "Checksums-Sha256"}
    dsc_file_hashes = {}
    with open(dsc_file, "rb") as fp:
        dsc_content = fp.read()
        for h in hashes.keys():
            dsc_file_hashes[h] = getattr(hashlib, h)(dsc_content).hexdigest()

    with open(changes_file, "r") as fp:
        # parse .changes file with deb822.Changes
        rel = DebChanges(fp)
        for h, field in hashes.items():
            # Update dsc file hashes per field
            for index in range(len(rel[field])):
                if rel[field][index]["name"] == dsc_file_name:
                    hashfieldname = h
                    # special-case md5 in Changes' file
                    if h == "md5":
                        hashfieldname = "md5sum"
                    # Update hashsum entry
                    rel[field][index][hashfieldname] = dsc_file_hashes[h]
                    # Update size entry
                    rel[field][index]["size"] = dsc_file_size
                elif rel[field][index]["name"].endswith('.buildinfo'):
                    # Remove .buildinfo files
                    del rel[field][index]

    with open(changes_file, "w") as fp:
        fp.write(rel.dump())


@cli.command()
@click.option("--dgit-sha")
@click.argument("yaml-sigs", type=click.Path(exists=True))
@click.argument("unsigned-dsc-file", type=click.Path(exists=True))
@click.argument("unsigned-changes-file", type=click.Path(exists=True))
def sign_dsc_and_changes_from_sigs(
    dgit_sha, yaml_sigs, unsigned_dsc_file, unsigned_changes_file
):
    """
    Apply signature to dsc, then to changes with sigs from yaml
    """
    with open(yaml_sigs, "r") as fp:
        try:
            metadata = yaml.safe_load(fp)
        except (yaml.composer.ComposerError, yaml.scanner.ScannerError):
            # there is more than one document, we have to skip until the yaml document
            metadata = None
            seen_yaml_separator = False
            yaml_lines = ''
            fp.seek(0)
            for line in fp.readlines():
                if seen_yaml_separator:
                    yaml_lines += line
                if line.strip() == '---':
                    seen_yaml_separator = True
            metadata = yaml.safe_load(yaml_lines)
            if not metadata:
                raise Exception('No valid Yaml found')

    # First sign the dsc
    sign_dsc_from_sigs(dgit_sha, metadata, unsigned_dsc_file)
    # Update happened in-place
    signed_dsc_file = unsigned_dsc_file

    # Then update the changes file with the new dsc
    update_changes_with_dsc(unsigned_changes_file, signed_dsc_file)
    # Update happened in-place
    updated_changes_file = unsigned_changes_file

    signed_changes_file = apply_inline_signature(
        updated_changes_file,
        metadata["DTag-changes-Hash"],
        metadata["DTag-changes-Signature"],
    )

    # Finally write it
    with open(unsigned_changes_file, "w") as fp:
        fp.write(signed_changes_file)


if __name__ == "__main__":
    cli()
