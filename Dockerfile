FROM debian:bullseye-slim

# python3-* for magic.py
# git-buildpackage
RUN apt-get update; \
    apt-get install -y \
      --no-install-recommends \
      python3-click \
      python3-yaml \
      python3-debian \
      git-buildpackage \
      pristine-tar \
      gnupg \
      wget \
      jq \
      dput-ng \
    ; \
    rm -Rf /var/lib/apt/

COPY ./mentors-https.json /etc/dput.d/profiles/

COPY ./magic.py /usr/local/bin/
