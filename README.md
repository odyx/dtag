# A deferred dsc signer and uploader

The goal of that Proof of Concept is to allow Debian packages' upload to the Debian archive in Salsa CI (Debian's Gitlab instance), ideally after all sorts of tests have run.

## Design

The main idea is to allow attachement of certain data to a git tag that steps in Salsa CI can recover to generate a signed Debian source package, that can be uploaded to the Debian archive automatically.

To achieve that, generation of the source package needs to happen as follows:

* once the git status is to your convenience (as you would before a `dgit` upload), git tag the repository.
* build the Debian source package as follows:
```
gbp buildpackage -S -d --unsigned-changes --git-ignore-branch
sed '/\.buildinfo$/d' -i ../*_source.changes
debsign ../*_source.changes
```

This will:
* Use `git-buildpackage`, with `-S` for source-only; `-d` to avoid the build-dependencies check, `--unsigned-changes` as we'll sign later; `--git-ignore-branch` to work on non-default branches
* Use `sed` to remove references' to `.buildinfo` files, that are bound to be different on Salsa CI
* Sign the resulting `_source.changes` file

Then. The right metadata needs to be attached to the `git tag`:

```
./magic.py sigs-from-changes ../*_source.changes -m "debian/${D_VERSION} release" | git tag -s debian/${D_VERSION} -F -
```

Push the tag and notes:
```
git push origin debian/${D_VERSION}
```

Let the pipeline do its magic
